#ifndef __READ_GPP_HPP__
#define __READ_GPP_HPP__

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include <envvar.hpp>

#include <tuple>

#define result_type std::vector<std::tuple<std::string, int>>

unsigned short is_library(std::string name);

//extract missing include from the output of g++
result_type readGpp(std::string output);

std::vector<std::string> get_includes(std::string file);

#endif