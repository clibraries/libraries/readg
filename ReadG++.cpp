#include <readG++.hpp>

#include <cutility.hpp>

#include <iostream>
#include <string>
#include <tuple>

#include <filesystem>
namespace fs = std::filesystem;

#include <envvar.hpp>


//take a possible library name, and return 0 if it is not a library (or not found),
//1 if the library is found in sources, but not compiled,
//and 2 if the library is found in sources and compiled (also mean that the header is in the include folder)
unsigned short is_library(std::string name){
    //check if the library is in sources
    for(fs::path lib: fs::directory_iterator(GetEnvVar("SRC_PATH")[0])){
        if(lib.filename() == name){
            //check if the library is compiled
            for(fs::path lib: fs::directory_iterator(GetEnvVar("LIBRARY_PATH")[0])){
                if(lib.filename() == name){
                    return 2;
                }
            }
            return 1;
        }
    }
    return 0;
}

//return true if string1 contains string2
bool is_included(std::string string1, std::string string2){
    for(size_t i = 0; i < string1.size(); i++){
        if(string1[i] == string2[0]){
            bool found = true;
            for(size_t j = 1; j < string2.size(); j++){
                if(string1[i + j] != string2[j]){
                    found = false;
                    break;
                }
            }
            if(found){
                return true;
            }
        }
    }
    return false;
}

result_type readGpp(std::string output){
    result_type includes;
    std::ifstream file(output);
    std::string line;
    while (std::getline(file, line)){        
        
        line = Utility::remove_all(line, '\0'); //remove all null characters
        //std::cout << "D:/library/src/menu/menu.hpp:8:10: fatal error: getkey.hpp: No such file or directory" << " " << std::string("D:/library/src/menu/menu.hpp:8:10: fatal error: getkey.hpp: No such file or directory").size() << std::endl;
        if(is_included(line, "fatal error: ")){
            std::string include = line.substr(line.find("fatal error: ") + 13);
            include = include.substr(0, include.find(":"));
            std::string libname = include.substr(0, include.find("."));
            std::tuple<std::string, int> t = {libname, is_library(libname)};
            includes.push_back(t);
        }
    }

    return includes;
}

//return all #include in the file as a vector of string (file is the path to the file)
std::vector<std::string> get_includes(std::string file){
    std::vector<std::string> includes;
    std::ifstream filestream(file);
    std::string line;
    unsigned ln = 0;
    while (std::getline(filestream, line)){
        if(is_included(line, "#include") && line.find("#include") == 0){
            if(is_included(line, "<")){
                std::string include = line.substr(line.find("<") + 1);
                include = include.substr(0, include.find(">"));
                includes.push_back(include);
            }
            else if(is_included(line, "\"")){
                std::string include = line.substr(line.find("\"") + 1);
                include = include.substr(0, include.find("\""));
                includes.push_back(include);
            }
            else{
                throw std::runtime_error("Error: #include not followed by \" or < in file " + file + ", on line " + std::to_string(ln));
            }
        }
        ln++;
    }
    return includes;
}

